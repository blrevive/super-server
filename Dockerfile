FROM php:8-alpine

RUN apk add supervisor
ADD docker/crontab.txt /crontab.txt
ADD docker/supervisor.conf /supervisor.conf
RUN /usr/bin/crontab /crontab.txt

COPY . /app
WORKDIR /app
RUN echo "[]" > hosts.json
CMD ["supervisord", "-c", "/supervisor.conf"]