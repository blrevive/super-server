<?php
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

require __DIR__ . '/vendor/autoload.php';

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public static $HOST_FILE = __DIR__ . '/hosts';

    public function registerBundles()
    {
        return [new Symfony\Bundle\FrameworkBundle\FrameworkBundle()];
    }

    protected function configureContainer(ContainerConfigurator $c): void
    {
    }
    
    protected function configureRoutes(RoutingConfigurator $routes) : void
    {
        $routes->add('list', '/')->methods(['GET'])->controller([$this, 'listHostsAction']);
        $routes->add('add', '/')->methods(['POST'])->controller([$this, 'addHostAction']);
        $routes->add('remove', '/')->methods(["DELETE"])->controller([$this, 'removeHostAction']);
        $routes->add('update', '/update')->methods(['GET'])->controller([$this, 'updateHostsAction']);
    }

    protected function GetHosts()
    {
        $c = file_get_contents(KERNEL::$HOST_FILE);
        $hosts = explode("\n", $c);
        return $hosts;
    }

    protected function WriteHosts($hosts)
    {
        $c = implode("\n", $hosts);
        file_put_contents(KERNEL::$HOST_FILE, $c);
    }

    public function listHostsAction()
    {
        return new Response(implode("\n", $this->GetHosts()));
    }

    public function addHostAction(Request $request)
    {
        $clientIP = $request->getClientIp();
        if(!filter_var($clientIP, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
        {
            return new Response("ipv6 is not supported!", 500);
        }

        $newHost = $request->getContent();

        if(!filter_var($newHost, FILTER_VALIDATE_URL))
        {
            return new Response("invalid host url", 500);
        }

        $domain = parse_url($url, PHP_URL_HOST);
        $newHostIP = gethostbyname($newHostIP);
        if($newHostIP != $ip)
        {
            return new Response("ip missmatch (".gethostbyname($newHostIPw)." != ".$ip.")", 500);
        }

        $hosts = $this->GetHosts();
        foreach($hosts as $host)
        {
            $hostIP = gethostbyname(parse_url($host, PHP_URL_HOST));
            if($hostIP == $newHostIP)
            {
                return new Response("host already registered", 500);
            }
        }

        array_push($hosts, $newHost);
        $this->WriteHosts($hosts);

        return new Response("host was added");
    }

    public function removeHostAction(Request $request)
    {
        $ip = $request->getClientIp();
        $hosts = $this->GetHosts();

        foreach($hosts as $i => $host)
        {
            if(gethostbyname(parse_url($host, PHP_URL_HOST)) == $ip)
            {
                unset($hosts[$i]);
                $this->WriteHosts($hosts);
                return new Response("host was deleted");
            }
        }

        return new Response("no matching host found", 500);
    }

    protected function url_exists($url) {
        return curl_init($url) !== false;
    }

    public function updateHostsAction()
    {
        $hosts = $this->GetHosts();
        
        foreach($hosts as $i => $host)
        {
            if(!$this->url_exists($host)) {
                unset($hosts[i]);
            }
        }

        $this->WriteHosts($hosts);

        return new Response("Hosts update succesfull!", 200);
    }
}

$kernel = new Kernel('dev', true);
if(!isset($argc) || $argc == 1)
{
    $request = Request::createFromGlobals();
    $response = $kernel->handle($request);
    $response->send();
    $kernel->terminate($request, $response);
} elseif($argv[1] == "update") {
    $resp = $kernel->updateHostsAction();
    print($resp->getContent());
} else {
    print("[ERROR] Unknown command: " . $argv[1] . "\n");
}